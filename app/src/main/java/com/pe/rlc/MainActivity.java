package com.pe.rlc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tviTexto;
    Button butOK, butCancelar;
    ImageView iviLogo;
    EditText eteInformacion;

    Spinner spiMeses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tviTexto = findViewById(R.id.tviTexto);
        butOK = findViewById(R.id.butOK);
        butCancelar = findViewById(R.id.butCancelar);
        iviLogo = findViewById(R.id.iviLogo);
        spiMeses = findViewById(R.id.spiMeses);
        eteInformacion = findViewById(R.id.eteInformacion);

        //Agregar evento al boton
        butOK.setOnClickListener(this);
        butCancelar.setOnClickListener(this);

        tviTexto.setText("Wewewewewesa");


        //llenarSpinnerListado();
        llenarSpinnerArray();

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.butOK:
                int numero = Integer.parseInt(eteInformacion.getText().toString());

                Toast.makeText(this, "Mensaje es: " + numero, Toast.LENGTH_SHORT).show();

                iviLogo.setImageDrawable(getResources().getDrawable(R.drawable.ow));


                break;

            case R.id.butCancelar:
                Toast.makeText(this, "Click desde boton Cancelar", Toast.LENGTH_SHORT).show();

                iviLogo.setImageDrawable(getResources().getDrawable(R.drawable.logo));
                break;

        }

        //Toast

        if (view.getId() == R.id.butOK) {
            Toast.makeText(this, "Click desde boton OK", Toast.LENGTH_SHORT).show();
        } else if (view.getId() == R.id.butCancelar) {
            Toast.makeText(this, "Click desde boton Cancelar", Toast.LENGTH_SHORT).show();
        }


    }


    private void llenarSpinnerListado() {
        List<String> lstMeses = new ArrayList<>();

        lstMeses.add("Enero");
        lstMeses.add("Febrero");
        lstMeses.add("Marzo");
        lstMeses.add("Abril");
        lstMeses.add("Mayo");
        lstMeses.add("Junio");
        lstMeses.add("Julio");
        lstMeses.add("Agosto");
        lstMeses.add("Septiembre");
        lstMeses.add("Octubre");
        lstMeses.add("Noviembre");
        lstMeses.add("Diciembre");


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, lstMeses);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spiMeses.setAdapter(dataAdapter);

    }

    private void llenarSpinnerArray() {

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.array_meses));

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spiMeses.setAdapter(dataAdapter);
    }


}
